import physicalEntityResolver from "./resolvers/physicalEntity";
import ewasResolver from "./resolvers/ewas";

const resolvers = {
  Query: {
      PhysicalEntity: (parent, args, context, info) => {
          let session = context.driver.session(),
            params = {dbId : args.dbId},
            query = `MATCH (pe:PhysicalEntity) WHERE pe.dbId = $dbId RETURN pe`;

          return session.run(query, params).then((result) => {
            const record = result.records[0].get("pe");
            return record;
          });
      }
  },
  PhysicalEntity: physicalEntityResolver,
  EntityWithAccessionedSequence: ewasResolver
};

export default resolvers;
